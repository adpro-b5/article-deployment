package adpro.article.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import adpro.article.core.Comment;
import adpro.article.core.JsComment;
import adpro.article.core.Writer;
import adpro.article.service.ArticleService;
import adpro.article.service.CommentService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ArticleRestController {
    @Autowired
    private ArticleService service;

    @Autowired
    private CommentService commentService;

    @PostMapping(value="/add-comment")
    public void createComment(@RequestBody JsComment comment) {
        Writer article = service.getWriter(Long.valueOf(comment.getArticle()));
        Comment newComment = new Comment();
        newComment.setArticle(article);
        newComment.setCommented(comment.getComment());

        commentService.addComment(newComment);
    }

    @GetMapping(value="/article")
    public List<Writer> displayArticle() {
        List<Writer> articlesList = service.getWriters();
        return articlesList;
    }

    @PostMapping(value="/add-article")
    public void addArticle(@RequestBody Writer writer){
        service.addWriter(writer);
    }

    @GetMapping(value="/detail/{idArticle}")
    public List<List<String>> getArticle(@PathVariable(value="idArticle") Long idArticle) {
        Writer article = service.getWriter(idArticle);
        List<String> result1 = new ArrayList<>();
        result1.add(article.getFullname());
        result1.add(article.getTitle());
        result1.add(article.getEmail());
        result1.add(article.getBody());
        result1.add(String.valueOf(article.getId()));
        List<String> result2 = new ArrayList<>();
        for (Comment c : article.getListComment()) {
            result2.add(c.getCommented());
        }
        List<List<String>> result = new ArrayList<>();
        result.add(result1);
        result.add(result2);
        return result;
    }
}