package adpro.article.service;

import java.util.List;

import adpro.article.core.Comment;

public interface CommentService {
    public void addComment(Comment comment);
    public List<Comment> getComments(Long idWriter);
    public Comment getComment(Long id);
}