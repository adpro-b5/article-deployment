package adpro.article.service;

import java.util.List;

import adpro.article.core.Writer;

public interface ArticleService {
    public void addWriter(Writer writer);
    public boolean checkWriterExist(Writer writer);
    public List<Writer> getWriters();
    public Writer getWriter(Long id);
}