package adpro.article.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adpro.article.core.Comment;
import adpro.article.core.Writer;
import adpro.article.repository.ArticleRepository;
import adpro.article.repository.CommentRepository;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ArticleRepository articleRepository;

    public void addComment(Comment Comment) {
        Comment newComment = commentRepository.save(Comment);
    }

    public List<Comment> getComments(Long idWriter) {
        Writer temp = articleRepository.findById(idWriter).get();
        return temp.getListComment();
    }

    public Comment getComment(Long id) {
        return commentRepository.findById(id).get();
    }
}